﻿using ClosedXML.Excel;
using ExcelApp.Controllers;
using System.Text.RegularExpressions;

namespace ExcelApp
{
    internal static class DataManager
    {
        static ClientController clientController;
        static ApplicationController applicationController;
        static GoodsController goodsController;
        static DataManager()
        {
            clientController = new ClientController("Клиенты");
            applicationController = new ApplicationController("Заявки");
            goodsController = new GoodsController("Товары");
        }
        internal static void GetListOfGoodsByName()
        {
            Console.WriteLine("Укажите наименование товара:");
            string goodName;
            IXLRow good;
            while (true)
            {
                goodName = ConsoleExtension.RepeatUntiNotNullReadline();
                good = goodsController.GetGoodByName(goodName);
                if (good != null)
                    break;
            }
            
            IEnumerable<IXLRangeRow> findApplications = applicationController.GetApplicationsByGoodCode(good.Cell(1).Value.GetNumber());
            List<IXLRow> findClients = new List<IXLRow>();
            foreach (var application in findApplications)
            {
                var client = clientController.GetByCode(application.Cell(3).Value.GetNumber());
                findClients.Add(client);
            }
            foreach (var client in findClients)
            {
                foreach (var application in findApplications)
                {
                    Console.WriteLine($"Клиент: {client.Cell(2).Value} \n" +
                        $"Количество: {application.Cell(5).Value} \n" +
                        $"Цена: {good.Cell(4).Value} \n" +
                        $"Дата заказа: {application.Cell(6).Value.GetDateTime():d}\r\n");
                }
            }
            Console.Write("Для продолжения нажмите Enter...");
            Console.ReadLine();
        }

        internal static void EditContact()
        {

            Console.WriteLine("Укажите наименование клиента:");
            string clientName;
            IXLRow? client;
            while(true)
            {
                clientName = ConsoleExtension.RepeatUntiNotNullReadline();
                client = clientController.GetByName(clientName);
                if (client != null)
                    break;
            }
            Console.WriteLine($"Укажите новое контактное лицо клиента {clientName}:");
            string newClientName = ConsoleExtension.RepeatUntiNotNullReadline();
            clientController.EditClientContactName(client, newClientName);
            Console.ReadLine();
        }

        internal static void GetGoldenCLient()
        {
            Console.WriteLine("Укажите год,месяц:");
            string yearAndMonthString;
            while (true)
            {
                yearAndMonthString = ConsoleExtension.RepeatUntiNotNullReadline().Replace(" ","");
                Regex regex = new Regex(@"\d{4},\d{1,2}", RegexOptions.IgnoreCase);
                if (regex.IsMatch(yearAndMonthString))
                    break;
                ConsoleExtension.ColorWriteLine(ConsoleColor.Red, "Значение введено неверное. Пожалуйста введите значение в формате ГГГГ,ММ.");
            }
            var yearAndMonth = yearAndMonthString.Split(",").Select(int.Parse).ToArray();
            var goldenClientCode = applicationController.GetGoldenClientCodeByYearAndMonth(yearAndMonth);
            var result = clientController.GetByCode(goldenClientCode);
            Console.Write(result!.Cell(2).Value);
            Console.ReadLine();
        }
    }
}
