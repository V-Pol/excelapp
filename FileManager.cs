﻿using ClosedXML.Excel;

namespace ExcelApp
{
    internal static class FileManager
    {
        internal static XLWorkbook workbook = new XLWorkbook();
        internal static void OpenFile()
        {
            Console.ForegroundColor = ConsoleColor.White;
            var fullFileName = ConsoleExtension.RepeatUntiNotNullReadline();
            LoadFile(fullFileName);
        }

        private static void LoadFile(string fullFileName)
        {
            string fileName = Path.GetFileName(fullFileName);
            string xsltPath = Path.Combine(fullFileName);
            try
            {
                workbook = new XLWorkbook(xsltPath);
            }
            catch(FileNotFoundException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"Файл {fileName} не найден. " +
                    $"Проверьте правильность указания пути и имени файла и повторите попытку.");
            }
            catch (IOException ex) 
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"Файл {fileName} открыт в другой программе. " +
                    $"Закройте другую программу и повторите попытку.");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine($"Произошла ошибка при открытии файла {fileName}.");
            }
            finally
            {
                if (workbook.Worksheets.Count == 0)
                    OpenFile();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Файл открыт");
            Thread.Sleep(1000);
        }
    }
}
