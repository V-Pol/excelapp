﻿public static class ConsoleExtension
{
    /// <summary>
    /// Выводит цветное сообщение в консоль.
    /// </summary>
    /// <param name="color">Цвет текста</param>
    /// <param name="text">Выводимый текст</param>
    public static void ColorWriteLine(ConsoleColor color, string text)
    {
        Console.ForegroundColor = color;
        Console.WriteLine(text);
        Console.ResetColor();
    }

    /// <summary>
    /// Запрос ввода текста до тех пор пока пользователь не укажет значение
    /// </summary>
    /// <returns>Введённая строка текста</returns>
    public static string RepeatUntiNotNullReadline()
    {
        string? text = null;
        while (string.IsNullOrEmpty(text))
        {
            text = Console.ReadLine();
        }
        return text;
    }
}