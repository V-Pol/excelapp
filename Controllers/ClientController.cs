﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Globalization;

namespace ExcelApp.Controllers
{
    internal class ClientController : BaseController
    {
        public ClientController(string name) : base(name) { }

        internal IXLRow? GetByCode(double code)
        {
            var foundCell = worksheet.Column(1).Search(code.ToString(), CompareOptions.OrdinalIgnoreCase).First();
            return worksheet.Row(foundCell.Address.RowNumber);
        }

        internal IXLRow? GetByName(string name)
        {
            var foundCell = worksheet.Search(name, CompareOptions.OrdinalIgnoreCase).FirstOrDefault();
            if (foundCell == null)
            {
                ConsoleExtension.ColorWriteLine(ConsoleColor.Red, "Клиент с таким именем не найден. Повторите попытку.");
                return null;
            }
            return worksheet.Row(foundCell.Address.RowNumber);
        }

        internal void EditClientContactName(IXLRow client, string newName)
        {
            try
            {
                worksheet.Cell(client.Cell(4).Address).SetValue(newName);
                ConsoleExtension.ColorWriteLine(ConsoleColor.Green, 
                    "Контактные данные изменены успешно");
                FileManager.workbook.Save();
                Console.Write("Для продолжения нажмите Enter...");
            }
            catch 
            {
                ConsoleExtension.ColorWriteLine(ConsoleColor.Red,
                    "Не удалось изменить контактные данные клиента");            
            }
        }
    }
}
