﻿using ClosedXML.Excel;

namespace ExcelApp.Controllers
{
    internal abstract class BaseController
    {
        protected readonly IXLWorksheet worksheet;
        public BaseController(string name)
        {
            worksheet = FileManager.workbook.Worksheet(name);
        }
    }
}
