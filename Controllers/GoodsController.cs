﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Globalization;

namespace ExcelApp.Controllers
{
    internal class GoodsController : BaseController
    {
        public GoodsController(string name) : base(name) { }

        /// <summary>
        /// Получить товар по имени
        /// </summary>
        /// <param name="goodName">Имя товара</param>
        /// <returns>Найденная строка с товаром</returns>
        internal IXLRow GetGoodByName(string goodName)
        {
            var foundCell = worksheet.Search(goodName, CompareOptions.OrdinalIgnoreCase).FirstOrDefault();
            if (foundCell == null)
            {
                ConsoleExtension.ColorWriteLine(ConsoleColor.Red, "Товар с таким названием не найден. Повторите попытку.");
                return null;
            }
            return worksheet.Row(foundCell.Address.RowNumber);
        }
    }
}
