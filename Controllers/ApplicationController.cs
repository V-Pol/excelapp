﻿using ClosedXML.Excel;

namespace ExcelApp.Controllers
{
    internal class ApplicationController : BaseController
    {
        public ApplicationController(string name) : base(name) { }

        internal double GetGoldenClientCodeByYearAndMonth(int[] yearAndMonth)
        {
            var applications = GetApplicationsByYearAndMonth(yearAndMonth);
            var mostFrequentClientCode = applications
               .GroupBy(ap => ap.Cell(3).Value.GetNumber())
               .OrderByDescending(ap => ap.Count())
               .First().Key;
            return mostFrequentClientCode;
        }
        private IEnumerable<IXLRangeRow> GetApplicationsByYearAndMonth(int[] yearAndMonth)
        {
            var applications = worksheet.RangeUsed().RowsUsed().Skip(1);
            applications = applications.Where(
                ap => ap.Cell(6).GetDateTime().Year == yearAndMonth[0] &&
                ap.Cell(6).GetDateTime().Month == yearAndMonth[1]);
            return applications;
        }

        internal IEnumerable<IXLRangeRow> GetApplicationsByGoodCode(double goodCode)
        {
            var applications = worksheet.RangeUsed().RowsUsed().Skip(1);
            return applications.Where(a => a.Cell(2).Value.GetNumber() == goodCode);
        }
    }
}
