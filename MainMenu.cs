﻿using static System.Console;

namespace ExcelApp
{
    internal static class MainMenu
    {
        internal static void Show()
        {
            Title = "ExcelApp";
            ShowWelcome();
            ShowOpenFileMessage();

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = ShowMenu();
            }

            static void ShowWelcome()
            {
                ConsoleExtension.ColorWriteLine(ConsoleColor.Blue,
                    "Добро пожаловать в ExcelApp v1.0\r\n");
            }

            static void ShowOpenFileMessage()
            {
                WriteLine("Для начала работы укажите путь до файла:");
                FileManager.OpenFile();
            }

            static bool ShowMenu()
            {
                Clear();
                ConsoleExtension.ColorWriteLine(ConsoleColor.Cyan,
                    "Список действий");
                WriteLine("1) Получить информацию о клиентах по товару");
                WriteLine("2) Изменить контактную информацию о клиенте по названию");
                WriteLine("3) Получить информацию о золотом клиенте по году,месяцу");
                WriteLine("4) Выйти");
                Write("\r\nВыберите действие: ");

                switch (ReadLine())
                {
                    case "1":
                        DataManager.GetListOfGoodsByName();
                        return true;
                    case "2":
                        DataManager.EditContact();
                        return true;
                    case "3":
                        DataManager.GetGoldenCLient();
                        return true;
                    case "4":
                        return false;
                    default:
                        return true;
                }
            }
            
        }
    }
}
